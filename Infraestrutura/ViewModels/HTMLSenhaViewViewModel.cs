﻿using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels
{
    public class HTMLSenhaViewViewModel : IHTMLSenhaViewModel
    {
        public string Unidade { get; set; }

        public string Cabeçalho { get; set; }

        public string Prioridade { get; set; }

        public string Senha { get; set; }

        public string Serviço { get; set; }

        public string Data { get; set; }

        public string Hora { get; set; }

        public string Rodapé { get; set; }
    }
}
