﻿using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels
{
    public class ErroViewModel : IErroViewModel
    {
        public string Mensagem { get; set; }
    }
}
