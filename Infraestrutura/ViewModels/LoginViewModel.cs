﻿using Infraestrutura.Interfaces.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels
{
    public class LoginViewModel : ILoginViewModel
    {
        public string TokenAcesso { get; set; }
        public string TokenRecarregar { get; set; }

        public double TempoExpirar { get; set; }
    }
}
