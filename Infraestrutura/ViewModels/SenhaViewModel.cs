﻿using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels
{
    public class SenhaViewModel : ISenhaViewModel
    {
        public string Html { get; set; }

        public string Home { get; set; }
    }
}
