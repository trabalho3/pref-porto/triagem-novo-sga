﻿using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels
{
    public class ServiçoViewModel : IServiçoViewModel
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public int PrioridadeId { get; set; }
    }
}
