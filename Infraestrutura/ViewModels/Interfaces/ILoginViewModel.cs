﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Interfaces.ViewModels
{
    public interface ILoginViewModel
    {
        public string TokenAcesso { get; }
        public string TokenRecarregar { get; }
        public double TempoExpirar { get; }
    }
}
