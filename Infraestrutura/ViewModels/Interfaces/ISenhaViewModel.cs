﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels.Interfaces
{
    public interface ISenhaViewModel
    {
        string Html { get; }
        string Home { get; set; }
    }
}
