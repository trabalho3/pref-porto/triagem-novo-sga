﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels.Interfaces
{
    public interface IErroViewModel
    {
        string Mensagem { get; }
    }
}
