﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels.Interfaces
{
    public interface IServiçoViewModel
    {
        int Id { get; }
        int PrioridadeId { get; }
        string Nome { get; }
    }
}
