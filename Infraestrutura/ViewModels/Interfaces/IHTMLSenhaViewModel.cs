﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels.Interfaces
{
    public interface IHTMLSenhaViewModel
    {
        public string Unidade { get; }
        public string Cabeçalho { get;  }
        public string Prioridade { get; }
        public string Senha { get; }
        public string Serviço { get;}
        public string Data { get;}
        public string Hora { get; }
        public string Rodapé { get; }
    }
}
