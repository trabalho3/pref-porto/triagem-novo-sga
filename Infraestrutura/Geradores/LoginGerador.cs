﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Interfaces.ViewModels;
using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores
{
    public class LoginGerador : ILoginGerador
    {
        public ILoginViewModel GerarLogin(ITokenAPIModelo dadosAPIToken) => new LoginViewModel { TokenAcesso = dadosAPIToken.TokenAcesso, TokenRecarregar = dadosAPIToken.TokenRecarregar, 
            TempoExpirar = dadosAPIToken.SegundosExpirar - 100 };
    }
}
