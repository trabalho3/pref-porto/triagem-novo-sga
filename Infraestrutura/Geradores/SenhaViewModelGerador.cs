﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.ViewModels;
using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Text;

namespace Infraestrutura.Geradores
{
    public class SenhaViewModelGerador : ISenhaViewModelGerador
    {
        public ISenhaViewModel GerarViewModel(string html) => new SenhaViewModel { Html = html };

    }
}
