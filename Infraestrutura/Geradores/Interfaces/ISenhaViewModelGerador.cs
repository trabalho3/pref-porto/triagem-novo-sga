﻿using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores.Interfaces
{
    public interface ISenhaViewModelGerador
    {
        ISenhaViewModel GerarViewModel(string html);
    }
}
