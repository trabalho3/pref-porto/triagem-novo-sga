﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores.Interfaces
{
    public interface ISolicitarSenhaGerador
    {
        ISolicitarSenhaModelo<IClienteModelo> GerarSolicitação(int IDUnidade, int IDServiço, int prioridade); 
    }
}
