﻿using Infraestrutura.Interfaces.ViewModels;
using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores.Interfaces
{
    public interface ILoginGerador
    {
        ILoginViewModel GerarLogin(ITokenAPIModelo dadosAPIToken);
    }
}
