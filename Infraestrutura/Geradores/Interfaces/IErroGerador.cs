﻿using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores.Interfaces
{
    public interface IErroGerador
    {
        IErroViewModel GerarErroConhecido(string mensagem);
        IErroViewModel GerarErroDesconhecido();
    }
}
