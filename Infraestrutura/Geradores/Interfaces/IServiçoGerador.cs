﻿using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores.Interfaces
{
    public interface IServiçoGerador
    {
        IEnumerable<IServiçoViewModel> ObterServiçoViewModels(IEnumerable<IServiçoModelo> modelos, IEnumerable<IPrioridadeModelo> prioridades);
    }
}
