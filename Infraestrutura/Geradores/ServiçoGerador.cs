﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.ViewModels;
using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infraestrutura.Geradores
{
    public class ServiçoGerador : IServiçoGerador
    {
        public IEnumerable<IServiçoViewModel> ObterServiçoViewModels(IEnumerable<IServiçoModelo> modelos, IEnumerable<IPrioridadeModelo> prioridades)
        {
            var serviços = new List<ServiçoViewModel>();

            foreach (var modelo in modelos)
            {
                foreach (var prioridade in prioridades)
                {
                    serviços.Add(new ServiçoViewModel { Id = modelo.Id, Nome = $"{modelo.Nome} - {prioridade.Nome}", PrioridadeId = prioridade.Id});
                }
            }

            return serviços;
        }
    }
}
