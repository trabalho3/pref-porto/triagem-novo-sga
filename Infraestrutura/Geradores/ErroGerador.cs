﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.ViewModels;
using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores
{
    public class ErroGerador : IErroGerador
    {
        public IErroViewModel GerarErroConhecido(string mensagem) => new ErroViewModel { Mensagem = mensagem };
        public IErroViewModel GerarErroDesconhecido() => new ErroViewModel { Mensagem = "Algo inexperado ocorreu." };
    }
}
