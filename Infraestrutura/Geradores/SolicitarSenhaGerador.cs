﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores
{
    public class SolicitarSenhaGerador : ISolicitarSenhaGerador
    {
        public ISolicitarSenhaModelo<IClienteModelo> GerarSolicitação(int IDUnidade, int IDServiço, int prioridade)
        {
            var solicitação = new SolicitarSenhaModelo
            {
                UnidadeId = IDUnidade,
                ServiçoId = IDServiço,
                PrioridadeId = prioridade,
                Cliente = new ClienteModelo
                {
                    Nome = "Visitante",
                    Documento = "Visitante"
                }
            };

            return solicitação;
        }
    }
}
