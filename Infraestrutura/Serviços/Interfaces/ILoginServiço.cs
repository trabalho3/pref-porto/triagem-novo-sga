﻿using Infraestrutura.Interfaces.ViewModels;
using System.Threading.Tasks;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface ILoginServiço
    {
        Task<ILoginViewModel> FazerLogin();
        Task<ILoginViewModel> RecarregarLogin(string tokenRecarregar);
    }
}
