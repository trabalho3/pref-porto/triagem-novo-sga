﻿using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface ISenhasServiço
    {
        Task<IEnumerable<IServiçoViewModel>> ObterServiços(string token);
        Task<ISenhaModelo> GerarSenha(string token, int IDUnidade, int IDServiço, int prioridade);
    }
}
