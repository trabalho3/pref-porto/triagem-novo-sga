﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.Serviços.Base;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Singletons.Interfaces;
using Infraestrutura.ViewModels.Interfaces;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infraestrutura.Serviços
{
    public class SenhasServiço : ServiçoAutenticávelBase, ISenhasServiço
    {
        private readonly IVariáveisDeAmbienteSingleton _ambienteSingleton;
        private readonly IServiçoGerador _serviçoGerador;
        private readonly ISolicitarSenhaGerador _solicitarSenhaGerador;
        private readonly ISenhaViewModelGerador _senhaViewModelGerador;

        public SenhasServiço(IVariáveisDeAmbienteSingleton ambienteSingleton, IServiçoGerador serviçoGerador, ISolicitarSenhaGerador solicitarSenhaGerador, ISenhaViewModelGerador senhaViewModelGerador)
        {
            _ambienteSingleton = ambienteSingleton;
            _serviçoGerador = serviçoGerador;
            _solicitarSenhaGerador = solicitarSenhaGerador;
            _senhaViewModelGerador = senhaViewModelGerador;
        }

        public async Task<ISenhaModelo> GerarSenha(string token, int IDUnidade, int IDServiço, int prioridade) =>  await DistribuirSenha(token, IDUnidade, IDServiço, prioridade);

        public async Task<IEnumerable<IServiçoViewModel>> ObterServiços(string token)
        {
            var cliente = ObterClientHttpComToken(token);

            using var respostaServiços = await cliente.GetAsync($"{_ambienteSingleton.Servidor}/servicos");
            var respostaStringServiços = await respostaServiços.Content.ReadAsStringAsync();
            var respostaJsonDesserializadaServiços = JsonSerializer.Deserialize<ServiçoModelo[]>(respostaStringServiços);

            using var respostaPrioridades = await cliente.GetAsync($"{_ambienteSingleton.Servidor}/prioridades");
            var respostaStringPrioridades = await respostaPrioridades.Content.ReadAsStringAsync();
            var respostaJsonDesserializadaPrioridades = JsonSerializer.Deserialize<PrioridadeModelo[]>(respostaStringPrioridades);

            return _serviçoGerador.ObterServiçoViewModels(respostaJsonDesserializadaServiços, respostaJsonDesserializadaPrioridades);
        }

        private async Task<ISenhaModelo> DistribuirSenha(string token, int IDUnidade, int IDServiço, int prioridade)
        {
            var cliente = ObterClientHttpComToken(token);

            var json = _solicitarSenhaGerador.GerarSolicitação(IDUnidade, IDServiço, prioridade) as SolicitarSenhaModelo;
            var jsonSerializado = JsonSerializer.Serialize(json);
            var jsonContéudoHttp = new StringContent(jsonSerializado, Encoding.UTF8, "application/json");

            var resposta = await cliente.PostAsync($"{_ambienteSingleton.Servidor}/distribui", jsonContéudoHttp);
            var respostaString = await resposta.Content.ReadAsStringAsync();
            var jsonDesserializado = JsonSerializer.Deserialize<SenhaModelo>(respostaString);

            return jsonDesserializado;
        }
    }
}
