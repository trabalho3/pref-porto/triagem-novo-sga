﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Interfaces.ViewModels;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Singletons.Interfaces;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;


namespace Infraestrutura.Serviços
{
    public class LoginServiço : ILoginServiço
    {
        private readonly IVariáveisDeAmbienteSingleton _variáveisAmbiente;
        private readonly ILoginGerador _geradorLogin;

        public LoginServiço(IVariáveisDeAmbienteSingleton variáveisAmbiente, ILoginGerador geradorLogin)
        {
            _variáveisAmbiente = variáveisAmbiente;
            _geradorLogin = geradorLogin;
        }

        public async Task<ILoginViewModel> FazerLogin()
        {
            using var clienteHttp = new HttpClient();
            using var formulário = new MultipartFormDataContent();

            formulário.Add(new StringContent("password"), "grant_type");
            formulário.Add(new StringContent(_variáveisAmbiente.Usuário), "username");
            formulário.Add(new StringContent(_variáveisAmbiente.Senha), "password");
            var resposta = await InserirDadosClienteEMandarRequisição(clienteHttp, formulário);

            return resposta;
        }


        public async Task<ILoginViewModel> RecarregarLogin(string tokenRecarregar)
        {
            using var clienteHttp = new HttpClient();
            using var formulário = new MultipartFormDataContent();

            formulário.Add(new StringContent(tokenRecarregar), "refresh_token");
            formulário.Add(new StringContent("refresh_token"), "grant_type");

            return await InserirDadosClienteEMandarRequisição(clienteHttp, formulário);

        }

        private async Task<ILoginViewModel> InserirDadosClienteEMandarRequisição(HttpClient clienteHttp, MultipartFormDataContent formulário)
        {
            formulário.Add(new StringContent("2_19xzm2lkgsckcko4wkwow0kk84ggwskgksksoog0840skwcgko"), "client_id");
            formulário.Add(new StringContent("54lqy7pmjxc0cwkc0ow88ccgc44sw4ss8cow4w4cg8co0wog8o"), "client_secret");

            using var resposta = await clienteHttp.PostAsync($"{_variáveisAmbiente.Servidor}/token", formulário);
            var pao = await resposta.Content.ReadAsStringAsync();
            resposta.EnsureSuccessStatusCode();

            var respostaConteúdoString = await resposta.Content.ReadAsStringAsync();
            var respostaJson = JsonSerializer.Deserialize<TokenAPIModelo>(respostaConteúdoString);

            return _geradorLogin.GerarLogin(respostaJson);
        }
    }
}
