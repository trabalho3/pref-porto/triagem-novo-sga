﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Infraestrutura.Serviços.Base
{
    public abstract class ServiçoAutenticávelBase
    {
        protected static HttpClient ObterClientHttpComToken(string token)
        {
            var retorno = new HttpClient();
            retorno.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");

            return retorno;
        }
    }
}
