﻿using Infraestrutura.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Singletons
{
    public class VariáveisDeAmbienteSingletonProduçãoSingleton : IVariáveisDeAmbienteSingleton
    {
        private readonly EnvironmentVariableTarget _ambiente = EnvironmentVariableTarget.Process;

        public string IDCliente => Environment.GetEnvironmentVariable("SGAClienteID", _ambiente);

        public string ChaveCliente => Environment.GetEnvironmentVariable("SGAClienteChave", _ambiente);

        public string Usuário => Environment.GetEnvironmentVariable("SGAUsuario", _ambiente);

        public string Senha => Environment.GetEnvironmentVariable("SGASenha", _ambiente);

        public string Servidor => Environment.GetEnvironmentVariable("SGAServidor", _ambiente);
    }
}
