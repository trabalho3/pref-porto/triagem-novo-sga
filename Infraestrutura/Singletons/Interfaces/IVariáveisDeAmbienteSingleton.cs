﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Singletons.Interfaces
{
    public interface IVariáveisDeAmbienteSingleton
    {
        string IDCliente { get; }
        string ChaveCliente { get; }
        string Usuário { get; }
        string Senha { get; }
        string Servidor { get; }
    }
}
