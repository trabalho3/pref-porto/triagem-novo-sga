﻿using Infraestrutura.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Singletons
{
    public class VariáveisDeAmbienteDesenvolvimentoSingleton : IVariáveisDeAmbienteSingleton
    {
        private readonly EnvironmentVariableTarget _ambiente = EnvironmentVariableTarget.Process;
        public string IDCliente => Environment.GetEnvironmentVariable("SGAClienteID", _ambiente);

        public string ChaveCliente => Environment.GetEnvironmentVariable("SGAClienteChave", _ambiente);

        public string Usuário => "admin";

        public string Senha => Environment.GetEnvironmentVariable("SenhaNovoSGA", _ambiente);

        public string Servidor => "http://127.0.0.1:9000/api";

    }
}