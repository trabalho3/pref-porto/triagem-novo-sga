﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class TokenAPIModelo : ITokenAPIModelo
    {
        [JsonPropertyName("access_token")]
        public string TokenAcesso { get; set; }

        [JsonPropertyName("expires_in")]
        public int SegundosExpirar { get; set; }

        [JsonPropertyName("token_type")]
        public string TipoToken { get; set; }

        [JsonPropertyName("scope")]
        public string Scopo { get; set; }

        [JsonPropertyName("refresh_token")]
        public string TokenRecarregar { get; set; }
    }
}
