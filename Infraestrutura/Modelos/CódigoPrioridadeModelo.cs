﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class CódigoPrioridadeModelo
    {
        [JsonPropertyName("nome")]
        public string Nome { get; set; }
    }
}
