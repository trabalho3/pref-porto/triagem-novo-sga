﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class ServiçoModelo : IServiçoModelo
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("nome")]
        public string Nome { get; set; }

        [JsonPropertyName("descricao")]
        public string Descrição { get; set; }

        [JsonPropertyName("peso")]
        public int Peso { get; set; }

        [JsonPropertyName("ativo")]
        public bool Ativo { get; set; }

        [JsonPropertyName("macro")]
        public string Macro { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime Criação { get; set; }

        //[JsonPropertyName("updatedAt")]
        //public DateTime Atualização { get; set; }

        //[JsonPropertyName("deletedAt")]
        //public DateTime DeletadoEm { get; set; }
    }
}
