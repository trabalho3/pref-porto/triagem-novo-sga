﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Modelos.Interfaces
{
    public interface IServiçoModelo
    {
        int Id { get; }
        string Nome { get; }
        string Descrição { get; }
        int Peso { get; }
        bool Ativo { get; }
        string Macro { get; }
        DateTime Criação { get; }
    }
}
