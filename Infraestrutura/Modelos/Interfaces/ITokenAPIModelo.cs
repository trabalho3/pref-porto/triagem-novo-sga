﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Modelos.Interfaces
{
    public interface ITokenAPIModelo
    {
        string TokenAcesso {get; } 
        int SegundosExpirar {get; } 
        string TipoToken { get; }
        string Scopo { get; }
        string TokenRecarregar { get; }
    }
}
