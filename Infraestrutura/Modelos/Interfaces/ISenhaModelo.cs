﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Modelos.Interfaces
{
    public interface ISenhaModelo
    {
        int Id { get; }
        string Hash { get; }
        CódigoSenhaModelo Senha { get; }
        CódigoServiçoModelo Serviço { get; }
        DateTime Chegada { get; }
        CódigoPrioridadeModelo Prioridade { get; }
    }
}
