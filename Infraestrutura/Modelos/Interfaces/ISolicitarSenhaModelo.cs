﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Modelos.Interfaces
{
    public interface ISolicitarSenhaModelo<out T> where T: IClienteModelo
    {
        int UnidadeId { get; }
        int ServiçoId { get; }
        int PrioridadeId { get; }
        T Cliente { get; }

    }
}
