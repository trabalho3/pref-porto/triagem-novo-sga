﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Modelos.Interfaces
{
    public interface IClienteModelo
    {
        string Nome { get; }
        string Documento { get; }
    }
}
