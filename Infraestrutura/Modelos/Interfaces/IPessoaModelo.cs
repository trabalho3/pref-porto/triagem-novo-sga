﻿namespace Infraestrutura.Modelos.Interfaces
{
    public interface IPessoaModelo
    {
        int Id { get; }
        string Cpf { get; }
        string Telefone { get; }
    }
}