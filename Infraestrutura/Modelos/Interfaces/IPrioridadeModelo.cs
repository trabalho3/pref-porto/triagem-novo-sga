﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Modelos.Interfaces
{
    public interface IPrioridadeModelo
    {
        public int Id { get; }
        public string Nome { get;}
        public bool Ativo { get;}
        public int Peso { get; }
        public string Descrição { get; }
        public DateTime Criação { get; }
    }
}
