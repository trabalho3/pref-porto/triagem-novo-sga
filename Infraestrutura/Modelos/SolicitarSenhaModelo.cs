﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class SolicitarSenhaModelo : ISolicitarSenhaModelo<ClienteModelo>
    {
        [JsonPropertyName("unidade")]
        public int UnidadeId { get; set; }

        [JsonPropertyName("servico")]
        public int ServiçoId { get; set; }

        [JsonPropertyName("prioridade")]
        public int PrioridadeId { get; set; }

        [JsonPropertyName("cliente")]
        public ClienteModelo Cliente { get; set; }
    }
}
