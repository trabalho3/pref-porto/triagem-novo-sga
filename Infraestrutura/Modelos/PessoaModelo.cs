﻿using System.Text.Json.Serialization;
using Infraestrutura.Modelos.Interfaces;

namespace Infraestrutura.Modelos
{
    public class PessoaModelo : IPessoaModelo
    {
        public int Id { get; set; }

        [JsonPropertyName("cpf")]
        public string Cpf { get; set; }
        [JsonPropertyName("telefone")]
        public string Telefone { get; set; }
    }
}