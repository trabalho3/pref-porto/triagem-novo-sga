﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class CódigoSenhaModelo
    {
        [JsonPropertyName("format")]
        public string Código { get; set; }
    }
}
