﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class CódigoServiçoModelo
    {
        [JsonPropertyName("nome")]
        public string Serviço { get; set; }
    }
}
