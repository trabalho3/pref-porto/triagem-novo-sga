﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class PrioridadeModelo : IPrioridadeModelo
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("nome")]
        public string Nome { get; set; }

        [JsonPropertyName("ativo")]
        public bool Ativo { get; set; }

        [JsonPropertyName("peso")]
        public int Peso { get; set; }

        [JsonPropertyName("descricao")]
        public string Descrição { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime Criação { get; set; }
    }
}
