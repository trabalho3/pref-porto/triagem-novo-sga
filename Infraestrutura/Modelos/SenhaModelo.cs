﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class SenhaModelo : ISenhaModelo
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("hash")]
        public string Hash { get; set; }

        [JsonPropertyName("senha")]
        public CódigoSenhaModelo Senha { get; set; }

        [JsonPropertyName("servico")]
        public CódigoServiçoModelo Serviço { get; set; }

        [JsonPropertyName("dataChegada")]
        public DateTime Chegada { get; set; }

        [JsonPropertyName("prioridade")]
        public CódigoPrioridadeModelo Prioridade { get; set; }

    }
}
