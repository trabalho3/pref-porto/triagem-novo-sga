﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.Modelos
{
    public class ClienteModelo : IClienteModelo
    {
        [JsonPropertyName("nome")]
        public string Nome { get; set; }

        [JsonPropertyName("documento")]
        public string Documento { get; set; }
    }
}
