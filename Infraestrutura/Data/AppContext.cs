﻿using Infraestrutura.Modelos;
using Microsoft.EntityFrameworkCore;


namespace Infraestrutura.Data
{
    public class AppContext : DbContext
    {
        public AppContext(DbContextOptions<AppContext> options) : base(options)
        { }
        
        public DbSet<PessoaModelo> Pessoas { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PessoaModelo>().ToTable("Pessoa_tb");
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;
            optionsBuilder.UseSqlServer("Server=localhost;Database=sgaPessoa;" +
                                        "Trusted_Connection=True;MultipleActiveResultSets=true");
        }
    }
}