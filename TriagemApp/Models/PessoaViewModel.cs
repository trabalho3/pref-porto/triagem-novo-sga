﻿using System.ComponentModel.DataAnnotations;
using Infraestrutura.Modelos;

namespace TriagemApp.Models
{
    public class PessoaViewModel
    {
        [Required(ErrorMessage = "CPF é Obrigatorio")]
        public string Cpf { get; set; }
        [Required(ErrorMessage = "Telefone é Obrigatorio")]
        public string Telefone { get; set; }


        public PessoaModelo ToModel(string cpf, string telefone)
        {
            return new PessoaModelo()
            {
                Cpf = cpf,
                Telefone = telefone
            };
        }
    }
}
