﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infraestrutura.Geradores;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Serviços;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Singletons;
using Infraestrutura.Singletons.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AppContext = Infraestrutura.Data.AppContext;

namespace TriagemApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            
            services.AddScoped<IErroGerador, ErroGerador>();
            services.AddScoped<ILoginGerador, LoginGerador>();
            services.AddScoped<IServiçoGerador, ServiçoGerador>();
            services.AddScoped<ISolicitarSenhaGerador, SolicitarSenhaGerador>();
            services.AddScoped<ISenhaViewModelGerador, SenhaViewModelGerador>();

            services.AddScoped<ILoginServiço, LoginServiço>();
            services.AddScoped<ISenhasServiço, SenhasServiço>();
            
            services.AddDbContext<AppContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnection"), 
                b=> b.MigrationsAssembly("TriagemApp")
                ));
            using var provider = services.BuildServiceProvider();
            var context = provider.GetRequiredService<AppContext>();
            context.Database.Migrate();

#if DEBUG
            services.AddSingleton<IVariáveisDeAmbienteSingleton, VariáveisDeAmbienteDesenvolvimentoSingleton>();
#else
            services.AddSingleton<IVariáveisDeAmbienteSingleton, VariáveisDeAmbienteSingletonProduçãoSingleton>();
#endif
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
