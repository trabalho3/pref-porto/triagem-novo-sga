﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Infraestrutura.Exceptions.Base;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Interfaces.ViewModels;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços.Interfaces;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TriagemApp.Models;
using AppContext = Infraestrutura.Data.AppContext;
using EntityState = Microsoft.EntityFrameworkCore.EntityState;

namespace TriagemApp.Controllers
{
    public class HomeController : Controller
    {
        const string CHAVE_TOKEN_ACESSO = "TokenAcesso";
        const string CHAVE_TOKEN_RECARREGAR = "TokenRecarregar";
        const string CHAVE_TOKEN_EXPIRAR = "TempoExpirar";

        private readonly ILoginServiço _loginServiço;
        private readonly IErroGerador _geradorErros;
        private readonly ISenhasServiço _senhasServiço;
        private readonly ILogger<HomeController> _logger;
        private readonly AppContext _appContext;

        public HomeController(ILoginServiço loginServiço, IErroGerador geradorErros, ISenhasServiço senhasServiço, AppContext appContext, ILogger<HomeController> logger)
        {
            _loginServiço = loginServiço;
            _geradorErros = geradorErros;
            _senhasServiço = senhasServiço;
            _appContext = appContext;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Index(PessoaViewModel pessoaViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Index), pessoaViewModel);
            }

            var resultado = pessoaViewModel.ToModel(pessoaViewModel.Cpf, pessoaViewModel.Telefone);
            //Salvar ou Atualizar dados das Pessoas no Banco  
            await SalvarPessoa(resultado);
            
            var dadosLogin = await _loginServiço.FazerLogin();
            var servicos = await _senhasServiço.ObterServiços(dadosLogin.TokenAcesso);
            

            
            CriarCookie(dadosLogin);
            return View(nameof(Painel),servicos);
        }

        private async Task SalvarPessoa(PessoaModelo resultado)
        {
            var checagem = await _appContext.Pessoas
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Cpf.Equals(resultado.Cpf));
            if(checagem is null)
                _appContext.Pessoas.Add(resultado);
            else
            {
                resultado.Id = checagem.Id;
                _appContext.Entry(resultado).State = EntityState.Modified;
            }

            await _appContext.SaveChangesAsync();
        }
        
        public async Task<IActionResult> Painel()
        {
            return View();
        }

        [HttpGet("{id}/{prioridade}")]
        public async Task<IActionResult> GerarSenha(int id, int prioridade)
        {
            string token = await ObterTokenMaisAtual();
            var senha = await _senhasServiço.GerarSenha(token, 1, id, prioridade);

            return View(senha);
        }

        public IActionResult Error()
        {
            var exceção = HttpContext.Features.Get<IExceptionHandlerFeature>();

            if(exceção.Error is ExceptionBase) return View(_geradorErros.GerarErroConhecido(exceção.Error.Message));
            return View(_geradorErros.GerarErroDesconhecido());
        }

        private void CriarCookie(ILoginViewModel dadosLogin)
        {

            var valorTokenAcesso = dadosLogin.TokenAcesso;
            var valorTokenRecarregar = dadosLogin.TokenRecarregar;
            var valorTokenExpirar = dadosLogin.TempoExpirar;

            var opçõesCookie = new CookieOptions
            {
                Expires = DateTime.Now.AddDays(1)
            };

            Response.Cookies.Append(CHAVE_TOKEN_ACESSO, valorTokenAcesso, opçõesCookie);
            Response.Cookies.Append(CHAVE_TOKEN_RECARREGAR, valorTokenRecarregar, opçõesCookie);
            Response.Cookies.Append(CHAVE_TOKEN_EXPIRAR, DateTime.Now.AddSeconds(valorTokenExpirar).ToString(), opçõesCookie);
        }

        private async Task<string> ObterTokenMaisAtual()
        {
            string token;
            var expiraçãoCookie = DateTime.Parse(HttpContext.Request.Cookies[CHAVE_TOKEN_EXPIRAR]);
            if (DateTime.Now > expiraçãoCookie)
            {
                var dadosLogin = await _loginServiço.RecarregarLogin(HttpContext.Request.Cookies[CHAVE_TOKEN_RECARREGAR]);
                CriarCookie(dadosLogin);
                token = dadosLogin.TokenAcesso;
            }
            else
            {
                token = HttpContext.Request.Cookies[CHAVE_TOKEN_ACESSO];
            }

            return token;
        }
    }
}
